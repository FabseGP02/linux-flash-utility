# Linux flash utility

A script-tool to download and/or install a given iso for either a x86_64- or rpi-system; currently the list of distros is limited to artix, alpine and freebsd, but can also pull directly from link or use a local iso.


## Running script

```bash
git clone https://gitlab.com/FabseGP02/linux-flash-utility.git
cd linux-flash-utility
./iso-utility.sh -m -a -d -p

Usage: 
  -m "download" or "install"
  -a "x86" or "rpi"
  -d "alpine", "artix", "freebsd" or local/online path
  -p e.g. "/dev/sdc"; only when -m install
Examples:
  ./iso-utility.sh -m download -a x86 -d artix
  ./iso-utility.sh -m download -a x86 -d https://download.artixlinux.org/iso/artix.iso
  ./iso-utility.sh -m install -a rpi -d /home/USERNAME/alpine.iso -p /dev/sdc
  ./iso-utility.sh -m install -a rpi -d freebsd -p /dev/sdc
```

## TODO
In no given order:

- [X] Automatic pull the latest iso listed within the database; currently pulls the first match
- [ ] Better GUI prompts


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[GPL](https://choosealicense.com/licenses/gpl-3.0/)
