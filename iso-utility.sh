#!/usr/bin/bash

# Static variables
  
  BEGINNER_DIR=$(pwd) 
  LIST="index.html" 
  ARCHITECTURE=""
  set -Ceu 
  trap user_exit SIGINT
  stty -echoctl 
  source distro_list
  if [[ "$(pacman -Qs opendoas)" ]]; then SU_COMMAND="doas"; 
  elif ! [[ "$(pacman -Qs opendoas)" ]] && [[ "$(pacman -Qs --color always "sudo" | grep "local" | grep "sudo ")" == "" ]]; then SU_COMMAND="sudo"; fi

#----------------------------------------------------------------------------------------------------------------------------------

# Functions

  usage() {
    cat <<EOF

  Linux flash utility
  Description: Uility to download and or flash a given iso
  Usage: 
    -m "download" or "install"
    -a "x86" or "rpi"
    -d ${DISTRO_LIST_OUTPUT[*]} or local/online path
    -p e.g. "/dev/sdc"; only when -m install
  Examples:
    ./iso-utility.sh -m download -a x86 -d artix
    ./iso-utility.sh -m download -a x86 -d https://download.artixlinux.org/iso/artix.iso
    ./iso-utility.sh -m install -a rpi -d /home/USERNAME/alpine.iso -p /dev/sdc
    ./iso-utility.sh -m install -a rpi -d freebsd -p /dev/sdc
EOF
  exit 2
  }

  user_exit() {
    echo 
    echo 
    echo "User abrupted script!"
  }

#----------------------------------------------------------------------------------------------------------------------------------

# Checks user-input

  while getopts "m:a:d:p:?h" opt; do
    case $opt in 
      m) MODE=$OPTARG
         case $MODE in 
           download|install);;
           *) echo ; echo "Invalid mode! Either \"download\" or \"install"\"; exit 2;;
         esac;;  
      a) ARCHITECTURE=$OPTARG
         case $ARCHITECTURE in 
           x86|rpi);;
           *) echo; echo "Invalid architecture! Either \"x86\" or \"rpi\""; exit 2;;
         esac;;  
      d) DISTRO=$OPTARG
         if printf '%s\0' "${DISTRO_LIST[@]}" | grep -Fxqz -- "$DISTRO"; then :; else      
           case $DISTRO in 
             *"http"*|*"home"*);;
             *) echo ; echo "Invalid distro! Either "${DISTRO_LIST_OUTPUT[*]}""; exit 2;;
           esac
         fi;;
      p) DRIVE_PATH=$OPTARG 
         DRIVE_LABEL="${DRIVE_PATH:5}"
         if [[ "$MODE" == "install" ]]; then 
           if lsblk -f | grep -wq "$DRIVE_LABEL"; then :; 
           else echo ; echo "Invalid drive!"; echo; lsblk; exit 2; fi
         fi;;
      h | *) usage;;
    esac
  done
  if [[ "$ARCHITECTURE" == "" || "$MODE" == "" || "$DISTRO" == "" ]]; then echo; echo "Missing parameters!"; usage; 
  else source distro_list; fi

#----------------------------------------------------------------------------------------------------------------------------------

# Downloading ISO
  
  if printf '%s\0' "${DISTRO_LIST[@]}" | grep -Fxqz -- "$DISTRO"; then :;
    wget -O index.html "$URL" -q
    if [[ -f "list.txt" ]]; then rm -f list.txt; fi
    grep -o "$STRING" index.html > list.txt 
    TARGET=$(tail -n 1 list.txt) 
    LINK=""$URL""$TARGET""   
  elif [[ "$DISTRO" == *"http"* ]]; then LINK="$DISTRO";
  elif [[ "$DISTRO" == *"home"* ]]; then TARGET="$DISTRO"; fi
  if ! [[ "$DISTRO" == *"home"* ]]; then wget -c -N --tries=0 --read-timeout=5 "$LINK" -q --show-progress --progress=bar:force 2>&1 || rm -f "$TARGET"; fi

#----------------------------------------------------------------------------------------------------------------------------------

# Formatting drives

  if [[ "$MODE" == "install" ]]; then
    if [[ "$TARGET" == *.img ]] || [[ "$TARGET" == *.iso ]]; then $SU_COMMAND dd if="$TARGET" of=/dev/"$DRIVE_LABEL" bs=4M status=progress;
    elif [[ "$TARGET" == *.img.xz ]]; then IMAGE=$(basename -s .xz "$TARGET"); 
      if ! [[ -f "$IMAGE" ]]; then $SU_COMMAND unxz -q "$TARGET"; fi
      $SU_COMMAND dd if="$IMAGE" of=/dev/"$DRIVE_LABEL" bs=4M status=progress;
    elif [[ "$TARGET" == *.tar.* ]]; then
      $SU_COMMAND parted /dev/"$DRIVE_LABEL" --script -- rm 1
      $SU_COMMAND wipefs -af /dev/"$DRIVE_LABEL"
      $SU_COMMAND parted /dev/"$DRIVE_LABEL" --script -- mklabel msdos
      $SU_COMMAND parted /dev/"$DRIVE_LABEL" --script -- mkpart primary 1 2g
      $SU_COMMAND parted /dev/"$DRIVE_LABEL" --script -- set 1 boot on
      $SU_COMMAND parted /dev/"$DRIVE_LABEL" --script -- set 1 lba on
      $SU_COMMAND parted /dev/"$DRIVE_LABEL" --script -- mkpart primary 2001 100%
      $SU_COMMAND mkfs.fat -F32 -I /dev/"$DRIVE_LABEL"1
      $SU_COMMAND mkfs.ext4 -F /dev/"$DRIVE_LABEL"2
    fi
  fi

#----------------------------------------------------------------------------------------------------------------------------------

# Extracts ISO + applies config-files / headless install

  if [[ "$MODE" == "install" ]]; then
    if [[ "$TARGET" == *.img ]] || [[ "$TARGET" == *.iso ]] || [[ "$TARGET" == *.img.xz ]]; then
      :
    elif [[ "$TARGET" == *.tar.* ]]; then
      if [[ "$TARGET" == *.tar.xz ]]; then TAR_COMMAND="tar -xf";
      elif [[ "$TARGET" == *.tar.gz ]]; then TAR_COMMAND="tar -zxf"; fi
      $SU_COMMAND mount /dev/"$DRIVE_LABEL"1 /mnt 
      cd /mnt || exit 
      FOLDER=$(basename -s .xz "$TARGET")
      if ! [[ -d "$FOLDER" ]]; then 
        if [[ "$DISTRO" == *"home"* ]]; then $SU_COMMAND $TAR_COMMAND "$TARGET"
        else $SU_COMMAND $TAR_COMMAND "$BEGINNER_DIR"/"$TARGET"; fi; fi
      cd "$BEGINNER_DIR" || exit
      if [[ "$DISTRO" == "alpine" ]]; then $SU_COMMAND cp packages/headless.apkovl.tar.gz /mnt; fi
      $SU_COMMAND umount /dev/"$DRIVE_LABEL"1
    fi
  fi
